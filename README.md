# Operations/Infrastructure Engineer (DevOps)

Dear Candidate,

First, we are happy you get this far on our hiring process, congratulations. We believe that interviews are probably not the best place to test the skills of a promising candidate. We created this little challenge for you to show your skills. We hope you enjoy it as much as we did.

This little challenge was created in a way to give you maximum freedom so that you can show us the best of your expertise.

That being said, good luck! Hope to hear from you soon.

## The Task

Your task is to create a development environment for a Flask (Python) application using Docker so that our Back-end Engineers can work on its development in a more standardized fashion.

Besides that, we expect you to create a Continuous Integration pipeline using GitLab CI. The pipeline must perform a security analisis, run the tests and finally build a Docker image.

### Bonus Point (not a requirement)

Provide a way to provision the necessary infrastructure and deploy the application to AWS, Azure or Google Cloud using Infra-as-Code (Cloudformation, Terraform, Ansible, Chef, Puppet, etc). You can create a free-tier account to do so.

## The Flask API

You're not expected to know Python to excel in this challenge. Following there is information about installing dependencies, performing security checks and, running tests.

### Create Virtual Environment

Create a virtual environment to manage dependencies in isolation from your system:

```bash
$ python -m venv venv
```

### Activate Virtual Environment

After you create a virtual environment, it is time to activate it:

```bash
$ source venv/bin/activate
```

### Install Dependencies

Now you have an isolated environment, you are safe to install the dependencies:

```bash
$ pip install -r requirements.txt
```

### Starting the Server

This is how you run a Flask API in _development mode_. To run that in _production mode_, remove the `FLASH_ENV` environment variable:

```
$ # development mode
$ FLASK_ENV="development" python api/app.py
```

```
$ # production mode
$ python api/app.py
```

Once the application is running, you can access it using cURL:

```bash
$ curl -H "Authorization: Bearer ThisShouldBeSecret" http://localhost:8000/
{"message":"Your Flask API server is flying!"}
```

### Running Security Analyzer (Bandit)

For this application, we have Bandit to perform security checks. Among other things, it looks for hard-coded passwords, which in fact should live in environment variables.

```bash
$ bandit -r api -c bandit.yml
```

### Running Tests (Pytest)

Finally, we use Pytest to run the test suit:

```bash
$ pytest
```

## FAQ: Frequently Asked Questions

### Am I supposed to know Python to excel in this challenge?

> No. You are being asked to (1) provide a development environment and (2) provide a GitLab CI pipeline.

### The security checks and/or the tests are not green?

> We encourage you to figure it out. You are free to change the application's code if you deem it necessary. If you feel stuck, though, write to ian.rodrigues@oowlish.com.

### Can I change the application's source code?

> Of course! You are free to change the application's code if you deem it necessary.

### Will I miss something if I do not do the "Bonus Point" tasks?

> Absolutely not! "Bonus points" tasks are meant for you to give you solid examples before our Technical Interview.

### How will I be evaluated?

> We expect to receive from you (1) a fully working development environment (using Docker Compose), (2) a GitLab CI pipeline with security checks, automated tests and automated Docker image build.
>
> We value well-written, clean code - without dead/unnecessary comments - and, good documentation. Above all other else, we do not like overengineering.
