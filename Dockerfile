FROM python:3.8-alpine

# Use this file as a starting point to create
# a production-ready Dockerfile for our Flask API.
#
# Bonus point: take advantage of Docker's Multi-Stage Build
# to make this single Dockerfile ready for both development
# and production environments.


# set the working directory in the container
WORKDIR /api

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt


COPY . /api




#Create a virtual environment to manage dependencies in isolation from your system:

RUN python -m venv venv

# Activate Virtual Environment

RUN source venv/bin/activate

# Starting the Server

RUN FLASK_ENV="development" 

RUN pytest

#RUN  cd /builds/mpandraded/recruit-master/ 

RUN  bandit -r api -c bandit.yml

EXPOSE 8000
# command to run on container start
CMD [ "python", "./api/app.py" ]


